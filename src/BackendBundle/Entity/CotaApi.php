<?php

namespace BackendBundle\Entity;

/**
 * CotaApi
 */
class CotaApi
{
    /**
     * @var integer
     */
    protected $idCotaApi;

    /**
     * @var string
     */
    protected $cota;

    /**
     * @var integer
     */
    protected $estado;

    /**
     * @var \BackendBundle\Entity\EventoApi
     */
    protected $idEventoApi;


    /**
     * Get idCotaApi
     *
     * @return integer
     */
    public function getIdCotaApi()
    {
        return $this->idCotaApi;
    }

    /**
     * Set cota
     *
     * @param string $cota
     *
     * @return CotaApi
     */
    public function setCota($cota)
    {
        $this->cota = $cota;

        return $this;
    }

    /**
     * Get cota
     *
     * @return string
     */
    public function getCota()
    {
        return $this->cota;
    }

    /**
     * Set estado
     *
     * @param integer $estado
     *
     * @return CotaApi
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return integer
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set idEventoApi
     *
     * @param \BackendBundle\Entity\EventoApi $idEventoApi
     *
     * @return CotaApi
     */
    public function setIdEventoApi(\BackendBundle\Entity\EventoApi $idEventoApi = null)
    {
        $this->idEventoApi = $idEventoApi;

        return $this;
    }

    /**
     * Get idEventoApi
     *
     * @return \BackendBundle\Entity\EventoApi
     */
    public function getIdEventoApi()
    {
        return $this->idEventoApi;
    }
}
