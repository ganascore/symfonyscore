<?php

namespace BackendBundle\Entity;

/**
 * DetalleApuesta
 */
class DetalleApuesta
{
    /**
     * @var integer
     */
    protected $idDatalleApuesta;

    /**
     * @var string
     */
    protected $matchId;

    /**
     * @var float
     */
    protected $cota;

    /**
     * @var integer
     */
    protected $estado;

    /**
     * @var \BackendBundle\Entity\Apuesta
     */
    protected $idApuesta;

    /**
     * @var \BackendBundle\Entity\Evento
     */
    protected $idEvento;

    /**
     * @var \BackendBundle\Entity\TipoApuesta
     */
    protected $idTipoApuesta;


    /**
     * Get idDatalleApuesta
     *
     * @return integer
     */
    public function getIdDatalleApuesta()
    {
        return $this->idDatalleApuesta;
    }

    /**
     * Set matchId
     *
     * @param string $matchId
     *
     * @return DetalleApuesta
     */
    public function setMatchId($matchId)
    {
        $this->matchId = $matchId;

        return $this;
    }

    /**
     * Get matchId
     *
     * @return string
     */
    public function getMatchId()
    {
        return $this->matchId;
    }

    /**
     * Set cota
     *
     * @param float $cota
     *
     * @return DetalleApuesta
     */
    public function setCota($cota)
    {
        $this->cota = $cota;

        return $this;
    }

    /**
     * Get cota
     *
     * @return float
     */
    public function getCota()
    {
        return $this->cota;
    }

    /**
     * Set estado
     *
     * @param integer $estado
     *
     * @return DetalleApuesta
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return integer
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set idApuesta
     *
     * @param \BackendBundle\Entity\Apuesta $idApuesta
     *
     * @return DetalleApuesta
     */
    public function setIdApuesta(\BackendBundle\Entity\Apuesta $idApuesta = null)
    {
        $this->idApuesta = $idApuesta;

        return $this;
    }

    /**
     * Get idApuesta
     *
     * @return \BackendBundle\Entity\Apuesta
     */
    public function getIdApuesta()
    {
        return $this->idApuesta;
    }

    /**
     * Set idEvento
     *
     * @param \BackendBundle\Entity\Evento $idEvento
     *
     * @return DetalleApuesta
     */
    public function setIdEvento(\BackendBundle\Entity\Evento $idEvento = null)
    {
        $this->idEvento = $idEvento;

        return $this;
    }

    /**
     * Get idEvento
     *
     * @return \BackendBundle\Entity\Evento
     */
    public function getIdEvento()
    {
        return $this->idEvento;
    }

    /**
     * Set idTipoApuesta
     *
     * @param \BackendBundle\Entity\TipoApuesta $idTipoApuesta
     *
     * @return DetalleApuesta
     */
    public function setIdTipoApuesta(\BackendBundle\Entity\TipoApuesta $idTipoApuesta = null)
    {
        $this->idTipoApuesta = $idTipoApuesta;

        return $this;
    }

    /**
     * Get idTipoApuesta
     *
     * @return \BackendBundle\Entity\TipoApuesta
     */
    public function getIdTipoApuesta()
    {
        return $this->idTipoApuesta;
    }
}
