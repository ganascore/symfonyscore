<?php

namespace BackendBundle\Entity;

/**
 * Apuesta
 */
class Apuesta
{
    /**
     * @var integer
     */
    protected $idApuesta;

    /**
     * @var float
     */
    protected $valorApostado;

    /**
     * @var float
     */
    protected $ganaciaTotal;

    /**
     * @var \DateTime
     */
    protected $fecha;

    /**
     * @var integer
     */
    protected $eventos;

    /**
     * @var integer
     */
    protected $estado;

    /**
     * @var \BackendBundle\Entity\Cliente
     */
    protected $idCliente;

    /**
     * @var \BackendBundle\Entity\Sucursal
     */
    protected $idSucursal;

    /**
     * @var \BackendBundle\Entity\Usuario
     */
    protected $idUsuario;


    /**
     * Get idApuesta
     *
     * @return integer
     */
    public function getIdApuesta()
    {
        return $this->idApuesta;
    }

    /**
     * Set valorApostado
     *
     * @param float $valorApostado
     *
     * @return Apuesta
     */
    public function setValorApostado($valorApostado)
    {
        $this->valorApostado = $valorApostado;

        return $this;
    }

    /**
     * Get valorApostado
     *
     * @return float
     */
    public function getValorApostado()
    {
        return $this->valorApostado;
    }

    /**
     * Set ganaciaTotal
     *
     * @param float $ganaciaTotal
     *
     * @return Apuesta
     */
    public function setGanaciaTotal($ganaciaTotal)
    {
        $this->ganaciaTotal = $ganaciaTotal;

        return $this;
    }

    /**
     * Get ganaciaTotal
     *
     * @return float
     */
    public function getGanaciaTotal()
    {
        return $this->ganaciaTotal;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     *
     * @return Apuesta
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set eventos
     *
     * @param integer $eventos
     *
     * @return Apuesta
     */
    public function setEventos($eventos)
    {
        $this->eventos = $eventos;

        return $this;
    }

    /**
     * Get eventos
     *
     * @return integer
     */
    public function getEventos()
    {
        return $this->eventos;
    }

    /**
     * Set estado
     *
     * @param integer $estado
     *
     * @return Apuesta
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return integer
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set idCliente
     *
     * @param \BackendBundle\Entity\Cliente $idCliente
     *
     * @return Apuesta
     */
    public function setIdCliente(\BackendBundle\Entity\Cliente $idCliente = null)
    {
        $this->idCliente = $idCliente;

        return $this;
    }

    /**
     * Get idCliente
     *
     * @return \BackendBundle\Entity\Cliente
     */
    public function getIdCliente()
    {
        return $this->idCliente;
    }

    /**
     * Set idSucursal
     *
     * @param \BackendBundle\Entity\Sucursal $idSucursal
     *
     * @return Apuesta
     */
    public function setIdSucursal(\BackendBundle\Entity\Sucursal $idSucursal = null)
    {
        $this->idSucursal = $idSucursal;

        return $this;
    }

    /**
     * Get idSucursal
     *
     * @return \BackendBundle\Entity\Sucursal
     */
    public function getIdSucursal()
    {
        return $this->idSucursal;
    }

    /**
     * Set idUsuario
     *
     * @param \BackendBundle\Entity\Usuario $idUsuario
     *
     * @return Apuesta
     */
    public function setIdUsuario(\BackendBundle\Entity\Usuario $idUsuario = null)
    {
        $this->idUsuario = $idUsuario;

        return $this;
    }

    /**
     * Get idUsuario
     *
     * @return \BackendBundle\Entity\Usuario
     */
    public function getIdUsuario()
    {
        return $this->idUsuario;
    }
}
