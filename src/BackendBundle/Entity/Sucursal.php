<?php

namespace BackendBundle\Entity;

/**
 * Sucursal
 */
class Sucursal
{
    /**
     * @var integer
     */
    protected $idSucursal;

    /**
     * @var string
     */
    protected $nombre;

    /**
     * @var float
     */
    protected $saldo = '0';

    /**
     * @var \BackendBundle\Entity\Empresa
     */
    protected $idEmpresa;


    /**
     * Get idSucursal
     *
     * @return integer
     */
    public function getIdSucursal()
    {
        return $this->idSucursal;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Sucursal
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set saldo
     *
     * @param float $saldo
     *
     * @return Sucursal
     */
    public function setSaldo($saldo)
    {
        $this->saldo = $saldo;

        return $this;
    }

    /**
     * Get saldo
     *
     * @return float
     */
    public function getSaldo()
    {
        return $this->saldo;
    }

    /**
     * Set idEmpresa
     *
     * @param \BackendBundle\Entity\Empresa $idEmpresa
     *
     * @return Sucursal
     */
    public function setIdEmpresa(\BackendBundle\Entity\Empresa $idEmpresa = null)
    {
        $this->idEmpresa = $idEmpresa;

        return $this;
    }

    /**
     * Get idEmpresa
     *
     * @return \BackendBundle\Entity\Empresa
     */
    public function getIdEmpresa()
    {
        return $this->idEmpresa;
    }
}
