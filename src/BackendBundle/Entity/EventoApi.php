<?php

namespace BackendBundle\Entity;

/**
 * EventoApi
 */
class EventoApi
{
    /**
     * @var integer
     */
    protected $idEventoApi;

    /**
     * @var string
     */
    protected $nombreLocal;

    /**
     * @var string
     */
    protected $nombreVitante;

    /**
     * @var string
     */
    protected $idTorneo;

    /**
     * @var \DateTime
     */
    protected $fecha;

    /**
     * @var string
     */
    protected $estado;


    /**
     * Get idEventoApi
     *
     * @return integer
     */
    public function getIdEventoApi()
    {
        return $this->idEventoApi;
    }

    /**
     * Set nombreLocal
     *
     * @param string $nombreLocal
     *
     * @return EventoApi
     */
    public function setNombreLocal($nombreLocal)
    {
        $this->nombreLocal = $nombreLocal;

        return $this;
    }

    /**
     * Get nombreLocal
     *
     * @return string
     */
    public function getNombreLocal()
    {
        return $this->nombreLocal;
    }

    /**
     * Set nombreVitante
     *
     * @param string $nombreVitante
     *
     * @return EventoApi
     */
    public function setNombreVitante($nombreVitante)
    {
        $this->nombreVitante = $nombreVitante;

        return $this;
    }

    /**
     * Get nombreVitante
     *
     * @return string
     */
    public function getNombreVitante()
    {
        return $this->nombreVitante;
    }

    /**
     * Set idTorneo
     *
     * @param string $idTorneo
     *
     * @return EventoApi
     */
    public function setIdTorneo($idTorneo)
    {
        $this->idTorneo = $idTorneo;

        return $this;
    }

    /**
     * Get idTorneo
     *
     * @return string
     */
    public function getIdTorneo()
    {
        return $this->idTorneo;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     *
     * @return EventoApi
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set estado
     *
     * @param string $estado
     *
     * @return EventoApi
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string
     */
    public function getEstado()
    {
        return $this->estado;
    }
}
