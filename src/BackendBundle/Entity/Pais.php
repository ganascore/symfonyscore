<?php

namespace BackendBundle\Entity;

/**
 * Pais
 */
class Pais
{
    /**
     * @var integer
     */
    protected $idPais;

    /**
     * @var string
     */
    protected $nombre;


    /**
     * Get idPais
     *
     * @return integer
     */
    public function getIdPais()
    {
        return $this->idPais;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Pais
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }
}
