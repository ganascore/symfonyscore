<?php

namespace BackendBundle\Entity;

/**
 * Torneo
 */
class Torneo
{
    /**
     * @var integer
     */
    protected $idTorneo;

    /**
     * @var string
     */
    protected $nombre;

    /**
     * @var \BackendBundle\Entity\Deporte
     */
    protected $idDeporte;

    /**
     * @var \BackendBundle\Entity\Pais
     */
    protected $idPais;


    /**
     * Get idTorneo
     *
     * @return integer
     */
    public function getIdTorneo()
    {
        return $this->idTorneo;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Torneo
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set idDeporte
     *
     * @param \BackendBundle\Entity\Deporte $idDeporte
     *
     * @return Torneo
     */
    public function setIdDeporte(\BackendBundle\Entity\Deporte $idDeporte = null)
    {
        $this->idDeporte = $idDeporte;

        return $this;
    }

    /**
     * Get idDeporte
     *
     * @return \BackendBundle\Entity\Deporte
     */
    public function getIdDeporte()
    {
        return $this->idDeporte;
    }

    /**
     * Set idPais
     *
     * @param \BackendBundle\Entity\Pais $idPais
     *
     * @return Torneo
     */
    public function setIdPais(\BackendBundle\Entity\Pais $idPais = null)
    {
        $this->idPais = $idPais;

        return $this;
    }

    /**
     * Get idPais
     *
     * @return \BackendBundle\Entity\Pais
     */
    public function getIdPais()
    {
        return $this->idPais;
    }
}
