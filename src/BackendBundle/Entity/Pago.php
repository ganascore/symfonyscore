<?php

namespace BackendBundle\Entity;

/**
 * Pago
 */
class Pago
{
    /**
     * @var integer
     */
    protected $idPago;

    /**
     * @var string
     */
    protected $tipoPago;

    /**
     * @var float
     */
    protected $monto;

    /**
     * @var \BackendBundle\Entity\Cliente
     */
    protected $idCliente;

    /**
     * @var \BackendBundle\Entity\Premio
     */
    protected $idPremio;

    /**
     * @var \BackendBundle\Entity\Sucursal
     */
    protected $idSucursal;

    /**
     * @var \BackendBundle\Entity\Usuario
     */
    protected $idUsuario;


    /**
     * Get idPago
     *
     * @return integer
     */
    public function getIdPago()
    {
        return $this->idPago;
    }

    /**
     * Set tipoPago
     *
     * @param string $tipoPago
     *
     * @return Pago
     */
    public function setTipoPago($tipoPago)
    {
        $this->tipoPago = $tipoPago;

        return $this;
    }

    /**
     * Get tipoPago
     *
     * @return string
     */
    public function getTipoPago()
    {
        return $this->tipoPago;
    }

    /**
     * Set monto
     *
     * @param float $monto
     *
     * @return Pago
     */
    public function setMonto($monto)
    {
        $this->monto = $monto;

        return $this;
    }

    /**
     * Get monto
     *
     * @return float
     */
    public function getMonto()
    {
        return $this->monto;
    }

    /**
     * Set idCliente
     *
     * @param \BackendBundle\Entity\Cliente $idCliente
     *
     * @return Pago
     */
    public function setIdCliente(\BackendBundle\Entity\Cliente $idCliente = null)
    {
        $this->idCliente = $idCliente;

        return $this;
    }

    /**
     * Get idCliente
     *
     * @return \BackendBundle\Entity\Cliente
     */
    public function getIdCliente()
    {
        return $this->idCliente;
    }

    /**
     * Set idPremio
     *
     * @param \BackendBundle\Entity\Premio $idPremio
     *
     * @return Pago
     */
    public function setIdPremio(\BackendBundle\Entity\Premio $idPremio = null)
    {
        $this->idPremio = $idPremio;

        return $this;
    }

    /**
     * Get idPremio
     *
     * @return \BackendBundle\Entity\Premio
     */
    public function getIdPremio()
    {
        return $this->idPremio;
    }

    /**
     * Set idSucursal
     *
     * @param \BackendBundle\Entity\Sucursal $idSucursal
     *
     * @return Pago
     */
    public function setIdSucursal(\BackendBundle\Entity\Sucursal $idSucursal = null)
    {
        $this->idSucursal = $idSucursal;

        return $this;
    }

    /**
     * Get idSucursal
     *
     * @return \BackendBundle\Entity\Sucursal
     */
    public function getIdSucursal()
    {
        return $this->idSucursal;
    }

    /**
     * Set idUsuario
     *
     * @param \BackendBundle\Entity\Usuario $idUsuario
     *
     * @return Pago
     */
    public function setIdUsuario(\BackendBundle\Entity\Usuario $idUsuario = null)
    {
        $this->idUsuario = $idUsuario;

        return $this;
    }

    /**
     * Get idUsuario
     *
     * @return \BackendBundle\Entity\Usuario
     */
    public function getIdUsuario()
    {
        return $this->idUsuario;
    }
}
