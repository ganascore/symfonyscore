<?php

namespace BackendBundle\Entity;

/**
 * RecargaSucursal
 */
class RecargaSucursal
{
    /**
     * @var integer
     */
    protected $idRecargaSucursal;

    /**
     * @var float
     */
    protected $monto;

    /**
     * @var \BackendBundle\Entity\Sucursal
     */
    protected $idSucursal;


    /**
     * Get idRecargaSucursal
     *
     * @return integer
     */
    public function getIdRecargaSucursal()
    {
        return $this->idRecargaSucursal;
    }

    /**
     * Set monto
     *
     * @param float $monto
     *
     * @return RecargaSucursal
     */
    public function setMonto($monto)
    {
        $this->monto = $monto;

        return $this;
    }

    /**
     * Get monto
     *
     * @return float
     */
    public function getMonto()
    {
        return $this->monto;
    }

    /**
     * Set idSucursal
     *
     * @param \BackendBundle\Entity\Sucursal $idSucursal
     *
     * @return RecargaSucursal
     */
    public function setIdSucursal(\BackendBundle\Entity\Sucursal $idSucursal = null)
    {
        $this->idSucursal = $idSucursal;

        return $this;
    }

    /**
     * Get idSucursal
     *
     * @return \BackendBundle\Entity\Sucursal
     */
    public function getIdSucursal()
    {
        return $this->idSucursal;
    }
}
