<?php

namespace BackendBundle\Entity;

/**
 * Premio
 */
class Premio
{
    /**
     * @var integer
     */
    protected $idPremio;

    /**
     * @var integer
     */
    protected $estado;

    /**
     * @var \DateTime
     */
    protected $fecha;

    /**
     * @var \DateTime
     */
    protected $fechaLimite;

    /**
     * @var \BackendBundle\Entity\Apuesta
     */
    protected $idApuesta;


    /**
     * Get idPremio
     *
     * @return integer
     */
    public function getIdPremio()
    {
        return $this->idPremio;
    }

    /**
     * Set estado
     *
     * @param integer $estado
     *
     * @return Premio
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return integer
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     *
     * @return Premio
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set fechaLimite
     *
     * @param \DateTime $fechaLimite
     *
     * @return Premio
     */
    public function setFechaLimite($fechaLimite)
    {
        $this->fechaLimite = $fechaLimite;

        return $this;
    }

    /**
     * Get fechaLimite
     *
     * @return \DateTime
     */
    public function getFechaLimite()
    {
        return $this->fechaLimite;
    }

    /**
     * Set idApuesta
     *
     * @param \BackendBundle\Entity\Apuesta $idApuesta
     *
     * @return Premio
     */
    public function setIdApuesta(\BackendBundle\Entity\Apuesta $idApuesta = null)
    {
        $this->idApuesta = $idApuesta;

        return $this;
    }

    /**
     * Get idApuesta
     *
     * @return \BackendBundle\Entity\Apuesta
     */
    public function getIdApuesta()
    {
        return $this->idApuesta;
    }
}
