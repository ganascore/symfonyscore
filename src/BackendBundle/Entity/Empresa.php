<?php

namespace BackendBundle\Entity;

/**
 * Empresa
 */
class Empresa
{
    /**
     * @var integer
     */
    protected $idEmpresa;

    /**
     * @var string
     */
    protected $nombre;

    /**
     * @var float
     */
    protected $tope = '0';


    /**
     * Get idEmpresa
     *
     * @return integer
     */
    public function getIdEmpresa()
    {
        return $this->idEmpresa;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Empresa
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set tope
     *
     * @param float $tope
     *
     * @return Empresa
     */
    public function setTope($tope)
    {
        $this->tope = $tope;

        return $this;
    }

    /**
     * Get tope
     *
     * @return float
     */
    public function getTope()
    {
        return $this->tope;
    }
}
