<?php

namespace BackendBundle\Entity;

/**
 * RecargaCliente
 */
class RecargaCliente
{
    /**
     * @var integer
     */
    protected $idRecargaCliente;

    /**
     * @var \DateTime
     */
    protected $fecha;

    /**
     * @var float
     */
    protected $monto;

    /**
     * @var \BackendBundle\Entity\Cliente
     */
    protected $idCliente;

    /**
     * @var \BackendBundle\Entity\Usuario
     */
    protected $idUsuario;


    /**
     * Get idRecargaCliente
     *
     * @return integer
     */
    public function getIdRecargaCliente()
    {
        return $this->idRecargaCliente;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     *
     * @return RecargaCliente
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set monto
     *
     * @param float $monto
     *
     * @return RecargaCliente
     */
    public function setMonto($monto)
    {
        $this->monto = $monto;

        return $this;
    }

    /**
     * Get monto
     *
     * @return float
     */
    public function getMonto()
    {
        return $this->monto;
    }

    /**
     * Set idCliente
     *
     * @param \BackendBundle\Entity\Cliente $idCliente
     *
     * @return RecargaCliente
     */
    public function setIdCliente(\BackendBundle\Entity\Cliente $idCliente = null)
    {
        $this->idCliente = $idCliente;

        return $this;
    }

    /**
     * Get idCliente
     *
     * @return \BackendBundle\Entity\Cliente
     */
    public function getIdCliente()
    {
        return $this->idCliente;
    }

    /**
     * Set idUsuario
     *
     * @param \BackendBundle\Entity\Usuario $idUsuario
     *
     * @return RecargaCliente
     */
    public function setIdUsuario(\BackendBundle\Entity\Usuario $idUsuario = null)
    {
        $this->idUsuario = $idUsuario;

        return $this;
    }

    /**
     * Get idUsuario
     *
     * @return \BackendBundle\Entity\Usuario
     */
    public function getIdUsuario()
    {
        return $this->idUsuario;
    }
}
