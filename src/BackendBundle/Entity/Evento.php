<?php

namespace BackendBundle\Entity;

/**
 * Evento
 */
class Evento
{
    /**
     * @var integer
     */
    protected $idEvento;

    /**
     * @var boolean
     */
    protected $estado;

    /**
     * @var string
     */
    protected $nombreLocal;

    /**
     * @var string
     */
    protected $nombreVicitante;

    /**
     * @var \DateTime
     */
    protected $fecha;

    /**
     * @var \BackendBundle\Entity\Torneo
     */
    protected $idTorneo;


    /**
     * Get idEvento
     *
     * @return integer
     */
    public function getIdEvento()
    {
        return $this->idEvento;
    }

    /**
     * Set estado
     *
     * @param boolean $estado
     *
     * @return Evento
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return boolean
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set nombreLocal
     *
     * @param string $nombreLocal
     *
     * @return Evento
     */
    public function setNombreLocal($nombreLocal)
    {
        $this->nombreLocal = $nombreLocal;

        return $this;
    }

    /**
     * Get nombreLocal
     *
     * @return string
     */
    public function getNombreLocal()
    {
        return $this->nombreLocal;
    }

    /**
     * Set nombreVicitante
     *
     * @param string $nombreVicitante
     *
     * @return Evento
     */
    public function setNombreVicitante($nombreVicitante)
    {
        $this->nombreVicitante = $nombreVicitante;

        return $this;
    }

    /**
     * Get nombreVicitante
     *
     * @return string
     */
    public function getNombreVicitante()
    {
        return $this->nombreVicitante;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     *
     * @return Evento
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set idTorneo
     *
     * @param \BackendBundle\Entity\Torneo $idTorneo
     *
     * @return Evento
     */
    public function setIdTorneo(\BackendBundle\Entity\Torneo $idTorneo = null)
    {
        $this->idTorneo = $idTorneo;

        return $this;
    }

    /**
     * Get idTorneo
     *
     * @return \BackendBundle\Entity\Torneo
     */
    public function getIdTorneo()
    {
        return $this->idTorneo;
    }
}
