<?php

namespace BackendBundle\Entity;

/**
 * Deporte
 */
class Deporte
{
    /**
     * @var integer
     */
    protected $idDeporte;

    /**
     * @var string
     */
    protected $nombre;


    /**
     * Get idDeporte
     *
     * @return integer
     */
    public function getIdDeporte()
    {
        return $this->idDeporte;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Deporte
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }
}
