<?php

namespace BackendBundle\Entity;

/**
 * Usuario
 */
class Usuario
{
    /**
     * @var integer
     */
    protected $idUsuario;

    /**
     * @var string
     */
    protected $nombre;

    /**
     * @var string
     */
    protected $apellido;

    /**
     * @var string
     */
    protected $cedula;

    /**
     * @var string
     */
    protected $correo;

    /**
     * @var string
     */
    protected $contrasenia;

    /**
     * @var string
     */
    protected $telefono;

    /**
     * @var string
     */
    protected $tipoRol;

    /**
     * @var \BackendBundle\Entity\Sucursal
     */
    protected $idSucursal;


    /**
     * Get idUsuario
     *
     * @return integer
     */
    public function getIdUsuario()
    {
        return $this->idUsuario;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Usuario
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set apellido
     *
     * @param string $apellido
     *
     * @return Usuario
     */
    public function setApellido($apellido)
    {
        $this->apellido = $apellido;

        return $this;
    }

    /**
     * Get apellido
     *
     * @return string
     */
    public function getApellido()
    {
        return $this->apellido;
    }

    /**
     * Set cedula
     *
     * @param string $cedula
     *
     * @return Usuario
     */
    public function setCedula($cedula)
    {
        $this->cedula = $cedula;

        return $this;
    }

    /**
     * Get cedula
     *
     * @return string
     */
    public function getCedula()
    {
        return $this->cedula;
    }

    /**
     * Set correo
     *
     * @param string $correo
     *
     * @return Usuario
     */
    public function setCorreo($correo)
    {
        $this->correo = $correo;

        return $this;
    }

    /**
     * Get correo
     *
     * @return string
     */
    public function getCorreo()
    {
        return $this->correo;
    }

    /**
     * Set contrasenia
     *
     * @param string $contrasenia
     *
     * @return Usuario
     */
    public function setContrasenia($contrasenia)
    {
        $this->contrasenia = $contrasenia;

        return $this;
    }

    /**
     * Get contrasenia
     *
     * @return string
     */
    public function getContrasenia()
    {
        return $this->contrasenia;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     *
     * @return Usuario
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return string
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set tipoRol
     *
     * @param string $tipoRol
     *
     * @return Usuario
     */
    public function setTipoRol($tipoRol)
    {
        $this->tipoRol = $tipoRol;

        return $this;
    }

    /**
     * Get tipoRol
     *
     * @return string
     */
    public function getTipoRol()
    {
        return $this->tipoRol;
    }

    /**
     * Set idSucursal
     *
     * @param \BackendBundle\Entity\Sucursal $idSucursal
     *
     * @return Usuario
     */
    public function setIdSucursal(\BackendBundle\Entity\Sucursal $idSucursal = null)
    {
        $this->idSucursal = $idSucursal;

        return $this;
    }

    /**
     * Get idSucursal
     *
     * @return \BackendBundle\Entity\Sucursal
     */
    public function getIdSucursal()
    {
        return $this->idSucursal;
    }
}
