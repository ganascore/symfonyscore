<?php

namespace BackendBundle\Entity;

/**
 * TipoApuesta
 */
class TipoApuesta
{
    /**
     * @var integer
     */
    protected $idTipoApuesta;

    /**
     * @var string
     */
    protected $nombre;


    /**
     * Get idTipoApuesta
     *
     * @return integer
     */
    public function getIdTipoApuesta()
    {
        return $this->idTipoApuesta;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return TipoApuesta
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }
}
