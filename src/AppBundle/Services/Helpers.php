<?php

/**
 * Created by IntelliJ IDEA.
 * User: andresfelipevidallopez
 * Date: 11/05/17
 * Time: 7:35 PM
 */

namespace AppBundle\Services;

use Symfony\Component\HttpFoundation\JsonResponse;


class Helpers
{
    public $jwt_auth;

    public function __construct($jwt_auth){
        $this->jwt_auth = $jwt_auth;
    }
    
    public function responseJson($data)
    {
        $response = new JsonResponse($data);

        return $response;
    }

}