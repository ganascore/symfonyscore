<?php
/**
 * Created by IntelliJ IDEA.
 * User: andresfelipevidallopez
 * Date: 11/05/17
 * Time: 8:20 PM
 */

namespace AppBundle\Services;

use Doctrine\ORM\UnexpectedResultException;
use Firebase\JWT\JWT;

class JwtAuthenticator
{
    public $manager;
    public $key;

    public function __construct($manager)
    {
        $this->key = "newGanaScore";
        $this->manager = $manager;
    }

    public function login($email, $pass)
    {


        $user = $this->manager->getRepository('BackendBundle:Usuario')->findOneBy(
            array(
                "correo" => $email,
                "contrasenia" => $pass
            )
        );

        if (is_object($user)) {
            $token = array(
                "sub" => $user->getIdUsuario(),
                "correo" => $user->getCorreo(),
                "nombre" => $user->getNombre(),
                "Apellido" => $user->getApellido(),
                "iat" => time(),
                "exp" => time() + (1 * 24 * 60 * 60)
            );

            $jwt = JWT::encode($token, $this->key, 'HS256');

            return array("status" => "200", "code" => "201", "message" => "Inicio Correcto", "data" => $jwt);

        } else {
            return array("status" => "200", "code" => "401", "message" => "Error al inicar", "data" => "");
        }
    }

    public function validatorToken($jwt)
    {
        $auth = false;

        try {
            $decode = JWT::decode($jwt, $this->key, array('HS256'));
        } catch (UnexpectedResultException $e) {
            dump($e);
        } catch (\DomainException $e){
            dump($e);
        }

        if (isset($decode->sub)) {
            $auth = true;
            return $auth;
        } else {
            return $auth;
        }

    }
}