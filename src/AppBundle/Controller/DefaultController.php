<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Email;


class DefaultController extends Controller
{

    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir') . '/..') . DIRECTORY_SEPARATOR,
        ]);
    }

    public function loginAction(Request $request) {
        $helpers = $this->get("app.helpers");
        $authenticator = $this->get("app.Authenticator");

        $json = $request->get("data", null);
        if ($json != null) {
            $params = json_decode($json);

            $email = (isset($params->email) ? $params->email : null);
            $pass = (isset($params->pass) ? $params-> pass : null);

            $emailContraint = new Email();
            $emailContraint->message = "Error de email";

            $validateEmail = $this->get("validator")->validate($email, $emailContraint);
            $response = null;
            if (count($validateEmail) == 0 && $pass != null){
                $response = $authenticator->login($email, $pass);
            } else {
                $response = $authenticator->login($email, $pass);
            }
            return $helpers->responseJson($response);
        } else {
            print_r("envide un json en el post");
            die();
        }

    }
    // mandar token por los headers
    public function testAction(Request $request)
    {
        $helpers = $this->get("app.helpers");
        $jwt_auth = $this->get("app.Authenticator");

        $hash = $request->get("auth", null);

        $response = $jwt_auth->validatorToken($hash);

        dump($response);
        die();
        $data = $this->getDoctrine()->getManager();
        $usuario = $data->getRepository('BackendBundle:Usuario')->findAll();
        return $helpers->responseJson($usuario);

    }

}
